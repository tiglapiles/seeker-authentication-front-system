import Vue from "vue";
import Vuex from "vuex";
import anthenticate from "./authenticate";
import "./verifyRules";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    anthenticate
  }
});
