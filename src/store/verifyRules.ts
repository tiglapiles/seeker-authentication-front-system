import { extend, localize } from "vee-validate";
import * as rules from "vee-validate/dist/rules";
const zh_CN = require("vee-validate/dist/locale/zh_CN.json");

// Add the required rule
for (let [rule, validation] of Object.entries(rules)) {
  extend(rule, {
    ...validation
  });
}

localize("zh_CN", zh_CN);
