import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: Login
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("../views/Signup.vue")
  },
  {
    path: "/resetpassword",
    name: "resetpassword",
    component: () => import("../views/ResetPassword.vue")
  },
  {
    path: "/resetconfirm",
    name: "resetconfirm",
    component: () => import("../views/ResetConfirm.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
